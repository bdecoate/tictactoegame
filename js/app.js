$(document).foundation();

$(window).scroll(function(e){
      scrollBackground();
});

 //  parallax scrolling
function scrollBackground(){
    var scrollAmount = $(window).scrollTop();
    if(scrollAmount/window.innerHeight < 1){
       $('#page1').css('background-position',
                       '50% ' + -(scrollAmount*1.25)+'px');
       $('#dragon').css('position','absolute');
       $('#dragon').css('bottom','' + -(scrollAmount*1.4+360)+'px');
       $('#dragon').css('left','' + (scrollAmount*.1)+'px');
    }
    else if(scrollAmount/window.innerHeight < 2){
       scrollAmount -= window.innerHeight;
       $('#page2').css('background-position', 
                       '50% ' + -(scrollAmount*0.25)+'px');
    }
}

// adapted from http://css-tricks.com/snippets/jquery/smooth-scrolling/
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
     && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 3500);
        return false;
      }
    }
  });
});
