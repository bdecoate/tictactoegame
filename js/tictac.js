//------------- game constructor ---------------------------------------------
function gameApp(){
    var app = this;

    app.defaultPiece = "-";          //default game board value
    app.c1 = "white";                //change to create tiled board
    app.c2 = "white";                //change to create tiled board
    app.xColor = "darkmagenta";       //color for X move
    app.xPiece = 'X';               //tile for X move
    app.oColor = "royalblue";        //color for O move
    app.oPiece = 'O';               //tile for O move
    app.aiDelay = ko.observable(250);//delay for AI moves

    //delay for restarting game after finishing
    app.restartDelay = ko.computed(function(){return app.aiDelay() * 4;});

    //flag if someone has won the game
    app.winner = false;

    //start/stop button text
    app.startReset = ko.observable("Start Game");

    // the gameboard size
    var n = 3;
    app.n = ko.observable(n);

    // number of pieces
    app.nSquared = ko.computed(function(){ return app.n()*app.n(); });

    // number of moves in current game
    app.numMoves = 0;

    //boolean flag to check if game is being played
    app.playing = ko.observable(false);

    //boolean flag to allow the game to clean up after victory
    app.cleanUpTime = ko.observable(false);

    //allow computers to play each other
    app.human = ko.observable(false);


    //------------- app stats -------------------------------------------------
    app.stats = { 'wins' :ko.observable(0),        //number of player wins
        'losses' :ko.observable(0),        //number of player losses
        'draws' :ko.observable(0),        //number of player draws
        'gamesPlayed' :ko.observable(0),  //number of games played
        'totalMoves' : ko.observable(0)   //total moves for all games
    };

    //shorthand games played variable
    app.stats.gp = ko.computed(function(){
        return app.stats.gamesPlayed() > 0 ? app.stats.gamesPlayed() : 1;
    });

    //average number of moves
    app.stats.avgMoves = ko.computed(function(){
        return (app.stats.totalMoves()/app.stats.gp()).toFixed(2);
    });

    //calculated win percentage
    app.stats.winPercent = ko.computed(function(){
        return (100*app.stats.wins()/app.stats.gp()).toFixed(2) + '%';
    });

    //calculated draw percentage
    app.stats.drawPercent = ko.computed(function(){
        return (100*app.stats.draws()/app.stats.gp()).toFixed(2) + '%';
    });

    //calculated loss percentage
    app.stats.lossPercent = ko.computed(function(){
        return (100*app.stats.losses()/app.stats.gp()).toFixed(2) + '%';
    });

    //array of move counts for all games
    app.stats.movesArray = ko.observableArray([]);
    //-------------------------------------------------------------------------

    //heatmap rect width
    app.w = ko.observable(40);

    //default max size for heatmap range
    app.defaultMax = 12;

    //upper bound for heatmap coloring
    app.maxClicks = ko.observable(app.defaultMax);

    //lower bound for heatmap coloring
    app.minClicks = ko.observable(0);

    //------------- app chart -------------------------------------------------
    app.chart = {};

    //number of past games to record
    app.chart.maxVals = 100;

    //the y positioning function for the chart
    //   updates upon board size changes
    app.chart.y = ko.computed(function(){
        return d3.scale.linear()
        .domain([0,app.nSquared()])
        .range([$('#movesChart').height(), 0]);
    });

    //the x positioning function for the chart
    app.chart.x = d3.scale.linear()
    .domain([0, app.chart.maxVals-1])
    .range([0, $('#movesChart').width()]);

    //the d3 line function for the chart
    app.chart.line = d3.svg.line()
    .x(function(d, i) { return app.chart.x(i); })
    .y(function(d, i) { return app.chart.y()(d); });


    //the selector for the chart
    app.chart.s = d3.select('#movesChart').append("g");

    //the x axis
    app.chart.xAxis = app.chart.s.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + app.chart.y()(0) + ")")
    .call(d3.svg.axis()
          .scale(app.chart.x)
          .orient("bottom")
          .tickSize(-$('#movesChart')
                    .height())
         );

         //the selector for the y axis
         app.chart.yAxisG = app.chart.s.append("g")
         .attr("class", "y axis")
         ;

         //the chart y axis -- will update when y updates
         app.chart.yAxis = ko.computed(function(){
             app.chart.yAxisG
             .call(d3.svg.axis()
                   .scale(app.chart.y())
                   .orient("left")
                   .tickSize(-$('#movesChart')
                             .width())
                  );
         });


         //the chart line
         app.chart.path = app.chart.s.append("g")
         .attr("clip-path", "url(#clip)")
         .append("path")
         .datum(app.stats.movesArray())
         .attr("class", "line")
         .attr("d", app.chart.line);

         //update the chart when the movesArray is updated
         app.chartUpdate = ko.computed(function(){
             if(app.stats.movesArray().length === 0) return;

             // redraw the line, and slide it to the left if necessary
             if(app.stats.movesArray().length == app.chart.maxVals-1){
                 app.chart.path
                 .attr("d", app.chart.line)
                 .attr("transform", null)
                 .transition()
                 .duration(app.aiDelay())
                 .ease("linear")
                 .attr("transform", "translate(" + app.chart.x(-1) + ",0)");
             } else {
                 app.chart.path
                 .attr("d", app.chart.line)
                 .attr("transform", null);
             }
         });

         //update the layout when the window is resized
         app.chart.resize = function(){
             app.chart.x = d3.scale.linear()
             .domain([0, app.chart.maxVals-1])
             .range([0, $('#movesChart').width()]);

             app.chart.xAxis
             .call(d3.svg.axis()
                   .scale(app.chart.x)
                   .orient("bottom")
                   .tickSize(-$('#movesChart')
                             .height())
                  );
                  app.chart.yAxisG
                  .call(d3.svg.axis()
                        .scale(app.chart.y())
                        .orient("left")
                        .tickSize(-$('#movesChart')
                                  .width())
                       );
         } ;

         //register the callback
         window.addEventListener("resize",function(e){
             app.chart.resize();
         });
         //---------------end game constructor---------------------------------

         //heatmap coloring function
         app.stats.heatMapColorMap = ko.computed(function(){
             return d3.scale.linear().domain([app.minClicks(),app.maxClicks()])
             .range(['gray','pink']);
         });

         //this is the base for the tic tac toe board
         //this n^2 element array is used to construct the
         app.gb = ko.computed(function(){
             var gb = [];
             for(var i = 0; i < app.nSquared(); i++){
                 gb.push({
                     "piece":ko.observable(app.defaultPiece),
                     "color":ko.observable(i%2?app.c1:app.c2),
                     "id":i,
                     "numClicksX":ko.observable(0),
                     "numClicksO":ko.observable(0)
                 });
             }
             return gb;
         });

         //this is the n x n tic tac toe board
         //it's computed as n arrays of n elements through this function
         //    which makes it easier to display the board via knockout
         app.gameboard = ko.computed(function(){
             var gb = app.gb();
             var gameboard = [];

             for(var i = 0; i < gb.length; i+=app.n()){
                 var tr = [];
                 for(var j = 0; j < app.n(); j++){
                     tr.push(gb[i+j]);
                 }
                 gameboard.push(tr);
             }
             return gameboard;
         });

         window.tictac = app;
}
//----------------------------------------------------------------------------


//clear the board pieces
//reset some of the states
gameApp.prototype.clearBoard = function(){
    var app = window.tictac;
    for(var i = 0; i < app.nSquared(); i++){
        app.gb()[i].piece(app.defaultPiece);
        app.gb()[i].color(i%2?app.c1:app.c2);
    }

    //if the game was cleared after a victory, record stats
    if(app.winner){
        app.stats.movesArray.push(app.numMoves);
        if(app.stats.movesArray().length > app.chart.maxVals){
            app.stats.movesArray.shift();
        }
    }

    app.numMoves = 0;
    app.cleanUpTime(false);
    app.winner = false;
};

//start button click handler
gameApp.prototype.startButton = function(){
    var app = window.tictac;
    if(app.playing()) {
        app.clearBoard();
    }
    app.playing(!app.playing());
    app.startReset(app.playing() ? "Stop Game" : "Start Game");

    //begin computer vs computer gameplay
    if(!app.human()){
        setTimeout(
            (function loop(){
            if(!app.playing()) return;

            //grab all valid moves
            var moves = app.gb().filter(function(d){
                return d.piece() == app.defaultPiece;
            });
            var move = null;
            var prob = Math.random();

            //randomly select the move that we will make
            if(prob < 0.375){
                move = moves[Math.floor(moves.length/2)];
            } else if(prob < 0.75){
                move = moves[moves.length-1];
            } else {
                move = moves[0];
            }

            app.setPiece(move);

            //make the next move after a delay
            setTimeout(loop,app.aiDelay()*2);
        }),250);
    }
    return;
};


//game board click handler
//  checks for valid moves
//  checks for victory
//  initiates computer's turn
//  checks for computer victory
gameApp.prototype.setPiece = function(data,evt){
    var app = window.tictac;
    if( !app.playing() ) return;
    if( app.cleanUpTime() ) return;
    var winner;

    //if this is a valid move
    if(data.piece() == app.defaultPiece){
        winner = app.playHumanTurn(data,app.xPiece);

        //clean up and record stats if winning move
        if(winner || app.numMoves == app.nSquared()){

            if(app.numMoves < app.nSquared()){ //a win
                app.stats.wins(app.stats.wins() + 1);
            } else { // a draw
                app.stats.draws(app.stats.draws() + 1);
            }

            //record stats
            app.stats.gamesPlayed(app.stats.gamesPlayed() + 1);
            app.stats.totalMoves(app.stats.totalMoves() + app.numMoves);

            //set the color for the move
            $("#"+data.id).css('background',app.xColor);

            //prevent more moves until cleanup is finished
            app.cleanUpTime(true);
            app.winner = true;

            //call cleanup routine after short delay
            setTimeout(app.clearBoard,app.restartDelay());
            return;
        }
    } else {
        //for an invalid click, toggle opacity to give feedback
        $("#"+data.id).fadeTo('250', 0.5).fadeTo('250', 1.0);
        return;
    }

    //prevent further moves until O player makes move
    app.cleanUpTime(true);

    //O player computer turn
    setTimeout(function(){
        app.playComputerTurn(app.oPiece);
    },app.aiDelay());
};

//handle X player turn
//update game board
//update stats
//check for winning move
gameApp.prototype.playHumanTurn = function(data,piece){
    var app = window.tictac;
    app.numMoves++;
    data.numClicksX(data.numClicksX() + 1);

    //update the heatmap domain
    if(data.numClicksX() > app.maxClicks()) app.maxClicks(app.maxClicks()*2);

    //set the piece and color for the board
    data.piece(piece);
    data.color(app.xColor);

    //check for winning move
    return app.checkIfWinner(data.id,data.piece());
};

//handle O player turn
//randomly select piece
//update game board
//update stats
//check for winning move
gameApp.prototype.playComputerTurn = function(piece){
    var app = window.tictac;
    if(app.numMoves === 0) return;

    app.numMoves++;

    var move = app.determineComputerMove();
    move.numClicksO(move.numClicksO() + 1);

    move.piece(piece);
    move.color(app.oColor);

    var winner = app.checkIfWinner(move.id,move.piece());

    //clean up and record stats if winning move
    if(winner || app.numMoves == app.nSquared()){
        if(app.numMoves < app.nSquared()){
            app.stats.losses(app.stats.losses() + 1);
        } else{
            app.stats.draws(app.stats.draws() + 1);
        }

        //recrod stats
        app.stats.gamesPlayed(app.stats.gamesPlayed() + 1);
        app.stats.totalMoves(app.stats.totalMoves() + app.numMoves);

        //set the color for the move
        $("#"+move.id).css('background',app.oColor);

        //prevent more moves until cleanup is finished
        app.winner = true;
        app.cleanUpTime(true);

        //call cleanup routine after short delay
        setTimeout(app.clearBoard,app.restartDelay());
        return;
    }

    //allow further moves to be made
    app.cleanUpTime(false);
};

//return a valid move that the O player will use
gameApp.prototype.determineComputerMove = function(){
    var app = window.tictac;

    //grab all valid moves
    var moves = app.gb().filter(function(d){
        return d.piece() == app.defaultPiece;
    });
    var move = null;
    var prob = Math.random();

    //randomly select a move
    if(prob < 0.25){
        move = moves[Math.floor(moves.length/2)];
    } else if(prob < 0.75){
        move = moves[moves.length-1];
    } else {
        move = moves[0];
    }

    return move;
};

//check for n in a row
gameApp.prototype.checkIfWinner = function(id,piece){
    var app = window.tictac;
    var gb = app.gb();
    var n = app.n();
    var i;

    //check up and down
    var numCorrect = 0;
    //up
    for(i = id; i >= (i%n) && i >= 0; i -= n){
        if(gb[i].piece() == piece) numCorrect++;
        else {
            break;
        }
    }
    //down
    for(i = id+n; i < n*n; i += n){
        if(gb[i].piece() == piece) numCorrect++;
        else {
            break;
        }
    }
    if(numCorrect == n) { return true;}

    //check left and right
    numCorrect = 0;
    //left
    for(i = id; i >= n*Math.floor(id/n); i--){
        if(gb[i].piece() == piece) numCorrect++;
        else {
            break;
        }
    }
    //right
    for(i = id+1; i < n*n && (i < id+(n-id%n)); i++){
        if(gb[i].piece() == piece) numCorrect++;
        else {
            break;
        }
    }
    if(numCorrect == n) { return true;}


    //check BL -> TR
    numCorrect = 0;
    //BL
    for(i = id; i <= n*(n-1); i+=n-1){
        if(gb[i].piece() == piece) numCorrect++;
        else {
            break;
        }
    }
    //TR
    for(i = id-(n-1); i >= n-1; i-= n-1){
        if(gb[i].piece() == piece) numCorrect++;
        else {
            break;
        }
    }
    if(numCorrect == n) { return true;}

    //check TL -> BR
    numCorrect = 0;
    //BR
    for(i = id; i < n*n; i+=n+1){
        if(gb[i].piece() == piece) numCorrect++;
        else {
            break;
        }
    }
    //TL
    for(i = id-(n+1); i >= 0; i-= n+1){
        if(gb[i].piece() == piece) numCorrect++;
        else {
            break;
        }
    }
    if(numCorrect == n) { return true;}
    return false;
};

//function to increase the gameboard size
gameApp.prototype.upSize = function(){
    var app = window.tictac;
    if(app.playing()) return;
    if(app.n() == 6) return;
    app.n(app.n()+1);
    app.maxClicks(app.defaultMax);
};

//function to decrease the gameboard size
gameApp.prototype.downSize = function(){
    var app = window.tictac;
    if(app.playing()) return;
    if(app.n() <= 3) return;
    app.n(app.n()-1);
    app.maxClicks(app.defaultMax);
};

//increase speed by reducing delay for AI moves
gameApp.prototype.upSpeed = function(){
    var app = window.tictac;
    if(app.aiDelay() > 0){
        app.aiDelay(app.aiDelay()-50);
    }
};

//decrease speed by increasing delay for AI moves
gameApp.prototype.downSpeed = function(){
    var app = window.tictac;
    if(app.aiDelay() < 1000){
        app.aiDelay(app.aiDelay()+50);
    }
};

// Activates knockout.js
ko.applyBindings(new gameApp());
